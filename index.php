<?php
header('Content-Type: text/html; charset=UTF-8');
  
$messages = [];
$errors = [];
$trimed=[];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

if (!empty($_COOKIE['save'])) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('save', '', 100000);
  // Если есть параметр save, то выводим сообщение пользователю.
  $messages[] = 'Спасибо, результаты сохранены.';
}

  $errors['name'] = !empty($_COOKIE['error_name']);
  $errors['email'] = !empty($_COOKIE['error_email']);
  $errors['bd'] = !empty($_COOKIE['error_bd']);
  $errors['sex'] = !empty($_COOKIE['error_sex']);
  $errors['limbs'] = !empty($_COOKIE['error_limbs']);
  $errors['contract'] = !empty($_COOKIE['error_contract']);

  
// if (!empty($errors)) {
//   exit();
// }

if ($errors['name']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_name', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните имя.</div>';
}
if ($errors['email']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_email', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните почту.</div>';
}
if ($errors['bd']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_bd', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните др.</div>';
}
if ($errors['sex']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_sex', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните пол.</div>';
}
if ($errors['limbs']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_limbs', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните конечности.</div>';
}
if ($errors['contract']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('error_contract', '', 100000);
  // Выводим сообщение.
  $messages[] = '<div ">Заполните контракт.</div>';
}

$values = array();
$values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['bd'] = empty($_COOKIE['bd_value']) ? '' : $_COOKIE['bd_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
$values['contract'] = empty($_COOKIE['contract_value']) ? '' : $_COOKIE['contract_value'];
$values['superpowers'] = array();
$values['superpowers'][0] = empty($_COOKIE['superpowers_value_0']) ? '' : $_COOKIE['superpowers_value_0'];
$values['superpowers'][1] = empty($_COOKIE['superpowers_value_1']) ? '' : $_COOKIE['superpowers_value_1'];
$values['superpowers'][2] = empty($_COOKIE['superpowers_value_2']) ? '' : $_COOKIE['superpowers_value_2'];
include('form.php');
}

else {
  // Проверяем ошибки.
  foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimed[$key] = trim($value);
	else
		$trimed[$key] = $value;
  $errors = FALSE;
  if ((empty($trimed['name']))) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_name', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }
  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimed['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_email', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  
  
  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimed['bd'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_bd', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('bd_value', $_POST['bd'], time() + 30 * 24 * 60 * 60);
  }
  
  if (!preg_match('/^[MFO]$/', $trimed['sex'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_sex', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }
  if (!preg_match('/^[2-4]$/', $trimed['limbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_limbs', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }
  if (!isset($trimed['contract'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('error_contract', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60);
  }
  setcookie('superpowers_value_0', '', 100000);
  setcookie('superpowers_value_1', '', 100000);
  setcookie('superpowers_value_2', '', 100000);
  
  foreach($trimed['superpowers'] as $super) {
    setcookie('superpowers_value_' . ($super - 1), 'true', time() + 30 * 24 * 60 * 60);
    }
 
  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('error_name', '', 100000);
    setcookie('error_email', '', 100000);
    setcookie('error_sex', '', 100000);
    setcookie('error_limbs', '', 100000);
    setcookie('error_bd', '', 100000);
    setcookie('error_contract', '', 100000);
  }

  setcookie('save', '1');

  header('Location: index.php');

  $user = 'u20401';
  $pass = '3661797';
  $db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


  try {
    $stmt1 = $db->prepare("INSERT INTO form_data (name, email, bd, sex, limbs, bio ) VALUES (?, ?, ?, ?, ?, ?)");
    $stmt1 -> execute([$trimed['name'], $trimed['email'], $trimed['bd'], $trimed['sex'], $trimed['limbs'], $trimed['bio']]);
    $stmt2 = $db->prepare("INSERT INTO form_supers (id_user, id_sups ) VALUES (?, ?)");
    $lastId = $db->lastInsertId();
    foreach ($trimed['superpowers'] as $super)
      $stmt2 -> execute([$lastId, $super]);
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }

  header('Location: ?save=1');
}

